package pl.sda.projektgrupowy.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.projektgrupowy.dto.CarUserDTO;
import pl.sda.projektgrupowy.entity.CarUser;
import pl.sda.projektgrupowy.repository.CarUserRepository;

@Slf4j
@RequiredArgsConstructor
@Transactional
@Service
public class CarUserService {

    private final CarUserRepository carUserRepository;

    public void createUser(CarUserDTO dto){
        log.info("create -> dto: {}", dto);
        CarUser createdUser = carUserRepository.save(new CarUser(dto.getUserId(),
                dto.getName(),
                dto.getSurname()));
        log.info("New car user created with id {}", createdUser.getUserId());
    }


}
