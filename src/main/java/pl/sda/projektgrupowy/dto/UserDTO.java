package pl.sda.projektgrupowy.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.sda.projektgrupowy.entity.UserRole;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter

public class UserDTO {

    private Long id;
    private String login;
    private String password;
    private Set<UserRole> roles;
}