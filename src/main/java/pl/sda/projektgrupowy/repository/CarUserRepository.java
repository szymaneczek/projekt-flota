package pl.sda.projektgrupowy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.projektgrupowy.entity.CarUser;

public interface CarUserRepository extends JpaRepository <CarUser, Long> {

}
