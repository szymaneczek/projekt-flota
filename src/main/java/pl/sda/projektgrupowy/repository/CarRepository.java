package pl.sda.projektgrupowy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.projektgrupowy.entity.Car;

public interface CarRepository extends JpaRepository <Car, Long> {

}


