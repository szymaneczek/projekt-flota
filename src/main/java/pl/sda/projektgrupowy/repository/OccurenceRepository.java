package pl.sda.projektgrupowy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.projektgrupowy.entity.Occurence;

public interface OccurenceRepository extends JpaRepository <Occurence, Long> {

}
