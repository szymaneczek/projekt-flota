package pl.sda.projektgrupowy;

import org.springframework.stereotype.Service;
import pl.sda.projektgrupowy.dto.CarUserDTO;
import pl.sda.projektgrupowy.entity.CarUser;
import pl.sda.projektgrupowy.repository.CarUserRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserFinder {

    private final CarUserRepository carUserRepository;
    public List<CarUserDTO> carUserList(){
        return carUserRepository.findAll().stream()
                .map(CarUser::toDto)
                .collect(Collectors.toList());
    }

    public UserFinder(CarUserRepository carUserRepository) {
        this.carUserRepository = carUserRepository;
    }
}
