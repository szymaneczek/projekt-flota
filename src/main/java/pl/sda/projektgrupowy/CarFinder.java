package pl.sda.projektgrupowy;

import org.springframework.stereotype.Service;
import pl.sda.projektgrupowy.dto.CarDTO;
import pl.sda.projektgrupowy.entity.Car;
import pl.sda.projektgrupowy.repository.CarRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarFinder {

    private final CarRepository carRepository;

    public List<CarDTO> carList() {
       return carRepository.findAll().stream()
                .map(Car::toDto)
                .collect(Collectors.toList());
    }

    public CarFinder(CarRepository carRepository) {
        this.carRepository = carRepository;
    }
}
