package pl.sda.projektgrupowy;

import org.springframework.stereotype.Service;
import pl.sda.projektgrupowy.dto.OccurenceDTO;
import pl.sda.projektgrupowy.entity.Occurence;
import pl.sda.projektgrupowy.repository.OccurenceRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OccurenceFinder {

    private final OccurenceRepository occurenceRepository;

    public List<OccurenceDTO> occurenceList(){
        return occurenceRepository.findAll().stream()
                .map(Occurence::toDto)
                .collect(Collectors.toList());
    }

    public OccurenceFinder(OccurenceRepository occurenceRepository) {
        this.occurenceRepository = occurenceRepository;
    }
}
