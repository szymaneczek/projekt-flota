INSERT INTO user_roles (id, role) VALUES (1, 'ROLE_USER');
INSERT INTO users (id, login, password) VALUES (1, 'user', '$2y$12$dolkmiS9lHpGt8MjLaTRvOtoSGVYMWrlVvmuQ0WOMo.MWo5pjfnXm');
INSERT INTO users (id, login, password) VALUES (3, 'Jarek', '$2y$12$5M6EVTtcCp1zhgjP96Z0O.YRqMkwkzwTH9LEUlErTSElAyhv0NW.C');
INSERT INTO users_roles (user_id, roles_id) VALUES (1, 1);
